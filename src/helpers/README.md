# Global shared helpers
The top-level helpers are common chunks of code that may or may not be used by other components and pages (component pages). Add any function that may be considered generic to this folder. If the need for larger structures arises, you can subdivide the pieces by functionality type.
