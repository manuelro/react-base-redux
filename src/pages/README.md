# Pages as components
The following architecture follows the page-as-component pattern. This consists in separating pages and components while using components as pages. In the page as component pattern the idea is to abstract as many standalone components as possible, building them in the most abstract way possible in order to make them reusable from any other component or page in the project.

Based on SOLID principles, a page as a component is a coordinator component that organizes the execution and invoking of more specific components. This follows the Single Responsibility Principle and makes the application easier to reason about.

## Folder structure
The folder structure is fractal, which means that the top level folder structure is replicated down every sub-page in the page tree as needed.

- Home.js (component)
- /Home (folder)
  - /components - custom components
  - /helpers - custom helpers
  - /hoc - custom HOC
  - /pages - subpages with the same structure as the parent page
  - /styles - custom styles for Home
  - /translations - the translations files for Home page
  - /assets - images, fonts, files specific for Home page
  - /tests - unit tests
