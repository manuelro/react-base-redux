import React, { Component } from 'react';
import { connect } from 'react-redux';

// Actions
import * as countriesActions from 'redux/countries/actions';
import * as leftDrawerActions from 'redux/left-drawer/actions';
import CountriesList from 'components/CountriesList';

/**
* Home - Component used as page
* @extends Component
*/
class Home extends Component{
  state = {}

  render(){
    const { dispatch } = this.props;

    return(
      <div>
        Homepage component

        <button onClick={() => dispatch( leftDrawerActions.error() )}>Throw an error</button>
        <button onClick={() => dispatch( leftDrawerActions.open() )}>Open left drawer</button>
        <button onClick={() => dispatch( leftDrawerActions.close() )}>Close left drawer</button>
        <button onClick={() => dispatch( leftDrawerActions.toggle() )}>Toggle left drawer</button>

        <button onClick={() => dispatch( countriesActions.fetch() )}>Perform async operation</button>
        <button onClick={() => dispatch( countriesActions.fetchWithError() )}>Perform async operation (with error)</button>

        <CountriesList data={this.props.state.country.countries}/>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { state };
}
const mapDispatchToProps = (dispatch) => {
  return { dispatch };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
