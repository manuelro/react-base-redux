# Higher Order Components
Follow the [conventions](https://facebook.github.io/react/docs/higher-order-components.html) created by Facebook in order to add and use HOCs in your application.
