import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import './styles/index.css';
import registerServiceWorker from './helpers/registerServiceWorker';

import store from './redux/shared/store';

import Root from 'components/Root';

ReactDOM.render(
  <Root store={store} />
  , document.getElementById('root'));
registerServiceWorker();
