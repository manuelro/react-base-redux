import React, { Component } from 'react';

export default class CountriesList extends Component{
  render(){
    const data = this.props.data || [];
    const countries = data.map((country, index) => (<li key={`country-${index}`}>{country.name}</li>));

    return(
      <ul>
        {countries.length ? countries : <li>Load countries async to show them</li>}
      </ul>
    );
  }
}
