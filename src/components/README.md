# Components
Components are units of code that encapsulate logic and presentational markup.

## Composability of components
In case a component needs to be composed of other components the following architecture is advised:
- Nav.js (component)
- /Nav (folder)
  - /components - custom subcomponents
  - /helpers - custom helpers
  - /hoc - custom HOC
  - /styles - custom styles for Home
  - /translations - the translations files for Home page
  - /assets - images, fonts, files specific for Home page
  - /tests - unit tests
