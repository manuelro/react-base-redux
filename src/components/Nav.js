import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Nav extends Component{
  render(){
    return(
      <div>
        <Link to={'/'}>Home</Link>
        <Link to='/figures'>Figures</Link>
        <Link to='/wagers'>Wagers</Link>
        <Link to='/transactions'>Transactions</Link>
        <Link to='/customers'>Customers</Link>
        <Link to='/history'>History</Link>
        <Link to='/calendar'>Calendar</Link>
        <Link to='/accounts'>Accounts</Link>
        <Link to='/account'>Account</Link>
        <Link to='/conversations'>Conversations</Link>
      </div>
    );
  }
}
