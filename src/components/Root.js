import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Components
import Layout from 'components/Layout';

// Pages
import Home from 'pages/Home';
import Figures from 'pages/Figures';
import Wagers from 'pages/Wagers';
import Transactions from 'pages/Transactions';
import Customers from 'pages/Customers';
import History from 'pages/History';
import Calendar from 'pages/Calendar';
import Accounts from 'pages/Accounts';
import Conversations from 'pages/Conversations';
import Account from 'pages/Account';

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <Layout>
        <Route exact path='/' component={Home}/>
        <Route path='/figures' component={Figures} />
        <Route path='/wagers' component={Wagers} />
        <Route path='/transactions' component={Transactions} />
        <Route path='/customers' component={Customers} />
        <Route path='/history' component={History} />
        <Route path='/calendar' component={Calendar} />
        <Route path='/accounts' component={Accounts} />
        <Route path='/conversations' component={Conversations} />
        <Route path='/account' component={Account} />
      </Layout>
    </Router>
  </Provider>
);

export default Root;
