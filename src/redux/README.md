# Redux as shared state repository
In order to share state between components, Redux is advised.

The folder structure approach will be a feature-based approach. Within each folder the `middlewares`, `reducers` and `stores` files are saved.

# Required top-level folders
Within this folder there will be a set of required folders for generic (globally shared) Redux functionality:
- /shared
  - /actions
  - /middlewares
  - /reducers
  - /stores

Other than these folders, any other feature folder will contain its own set of files with the very same names as the top level set of folders. In case more granularity is needed, each file will have a folder countrpart with the same name in which we'll place an specific file per each piece of functionality like the following:

- /countries
  - /actions.js
  - /actions
  - /middlewares.js
  - /middlewares
  - /reducers.js
  - /reducers
  - /stores.js
  - /stores
