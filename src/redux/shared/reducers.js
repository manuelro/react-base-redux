import { combineReducers } from 'redux';

import { countriesReducer } from 'redux/countries/reducers';
import { leftDrawerReducer } from 'redux/left-drawer/reducers';

export const reducers = combineReducers({
  country: countriesReducer,
  leftDrawer: leftDrawerReducer,
});
