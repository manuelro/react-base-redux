/**
 * logger - Sample logger middleware for Redux
 *
 * @param {type} store
 *
 * @return {type}
 */
export const logger = (store) => (next) => (action) => {
    // console.log('LOGGER: ', action);
    next(action);
}
