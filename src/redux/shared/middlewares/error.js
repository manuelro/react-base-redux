/**
 * error - Sample error middleware
 *
 * @param {type} store
 *
 * @return {type}
 */
export const error = (store) => (next) => (action) => {
    try {
      next(action);
    } catch (e) {
      console.log('Some error happened:', e.message);
    }
}
