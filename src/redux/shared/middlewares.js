import { applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

import { error } from './middlewares/error';
import { logger } from './middlewares/logger';

export const middlewares = applyMiddleware(error, logger, thunk);
