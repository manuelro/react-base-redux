import axios from 'axios';

export function fetch () {
  return (dispatch) => {
    dispatch({ type: 'COUNTRIES_FETCH_START'});
    axios.get('https://restcountries.eu/rest/v2/name/united')
      .then((res) => {
        dispatch({ type: 'COUNTRIES_FETCH_END', payload: res.data });
      })
      .catch((err) => dispatch({ type: 'COUNTRIES_FETCH_ERROR', payload: err }));
  }
}

export function fetchWithError () {
  return (dispatch) => {
    dispatch({ type: 'COUNTRIES_FETCH_START'});
    axios.get('https://restcountries.eu/rest/v2/nam/united')
      .then((res) => {
        dispatch({ type: 'COUNTRIES_FETCH_END', payload: res.data });
      })
      .catch((err) => dispatch({ type: 'COUNTRIES_FETCH_ERROR', payload: err }));
  }
}
