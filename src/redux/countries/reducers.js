const initialState = {
  fetching: false,
  fetched: false,
  countries: [],
  error: [],
}

export function countriesReducer (state = initialState, action){
  const { payload } = action;

  switch (action.type) {
    case 'COUNTRIES_FETCH_START':
      console.log('COUNTRIES_FETCH_START');
      return { ...state, fetching: true }
      break;
    case 'COUNTRIES_FETCH_END':
      console.log('COUNTRIES_FETCH_END');
      return { ...state, fetching: false, fetched: true, countries: payload }
      break;
    case 'COUNTRIES_FETCH_ERROR':
      console.log('COUNTRIES_FETCH_ERROR');
      return { ...state, fetching: false, error: payload }
      break;
    default:
  }

  return state;
}
