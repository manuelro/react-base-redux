export function error () {
  return (dispatch) => {
    dispatch({ type: 'LEFT_DRAWER_ERROR'});
  }
}

export function open () {
  return (dispatch) => {
    dispatch({ type: 'LEFT_DRAWER_OPEN'});
  }
}

export function close () {
  return (dispatch) => {
    dispatch({ type: 'LEFT_DRAWER_CLOSE'});
  }
}

export function toggle () {
  return (dispatch) => {
    dispatch({ type: 'LEFT_DRAWER_TOGGLE'});
  }
}
