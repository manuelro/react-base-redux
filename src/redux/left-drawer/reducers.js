export function leftDrawerReducer (state = true, action){
  const { payload } = action;

  switch (action.type) {
    case 'LEFT_DRAWER_ERROR':
      throw new Error('Left drawer error');
      break;
    case 'LEFT_DRAWER_OPEN':
      state = true;
      break;
    case 'LEFT_DRAWER_CLOSE':
      state = false;
      break;
    case 'LEFT_DRAWER_TOGGLE':
      state = !state;
      break;
    default:
  }

  return state;
}
